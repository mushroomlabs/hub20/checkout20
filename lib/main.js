import Vue from 'vue'

import CheckoutComponent from '@/components/Checkout'
import store from '@/store'
import {
  CHECKOUT_APP_SET_COMPLETED,
  CHECKOUT_APP_SET_CANCELED,
  CHECKOUT_APP_SET_EXPIRED,
} from '@/store'

class Checkout {
  constructor(serverUrl, storeId, charge, options) {
    this.serverUrl = serverUrl
    this.storeId = storeId
    this.charge = charge

    this.onCheckoutCompleted = options && options.onCheckoutCompleted
    this.onCheckoutExpired = options && options.onCheckoutExpired
    this.onCheckoutCanceled = options && options.onCheckoutCanceled

    this.onClose = options && options.onClose
    this.onOpen = options && options.onOpen

    this.container = null
    this.component = null
    this.unsubscribe = null
  }

  _buildCheckout() {
    let VueCheckout = Vue.extend(CheckoutComponent)

    this.component = new VueCheckout({store})
    this.component.$mount()
    this.component.$store.dispatch('initialize', {
      serverUrl: this.serverUrl,
      storeId: this.storeId,
      charge: this.charge,
    })

    this._subscribe(this.component)
  }

  _subscribe() {
    if (!this.component) {
      console.warn('No component instantiated. Can not subscribe.')
      return
    }

    this._unsubscribe = this.component.$store.subscribe((mutation, state) => {
      let payload = mutation.payload

      switch (mutation.type) {
        case CHECKOUT_APP_SET_COMPLETED:
          if (this.onCheckoutCompleted) {
            this.onCheckoutCompleted(state, payload)
          }
          break
        case CHECKOUT_APP_SET_CANCELED:
          if (this.onCheckoutCanceled) {
            this.onCheckoutCanceled(state, payload)
          }
          break
        case CHECKOUT_APP_SET_EXPIRED:
          if (this.onCheckoutExpired) {
            this.onCheckoutExpired(state, payload)
          }
          break
      }
    })
  }

  mount(selector) {
    let container = document.querySelector(selector)

    if (!container) {
      throw `${selector} is not a valid document selector`
    }

    if (!this.component) {
      this._buildCheckout()
    }
    this.container = container
    this.container.appendChild(this.component.$el)
  }

  unmount() {
    while (this.container.firstChild) {
      this.container.firstChild.remove()
    }
  }

  destroy() {
    if (this._unsubscribe) {
      this._unsubscribe()
      this._unsubscribe = null
    }

    if (this.component) {
      this.component.$destroy()
      this.component = null
    }

    this.unmount()
  }

  close() {
    if (!this.component) {
      throw 'Component not found. Can not close'
    }
    this.component.$store.dispatch('close')

    if (this.onClose) {
      this.onClose()
    }
  }

  open() {
    if (!this.component) {
      throw 'Component not found. Can not open'
    }

    this.component.$store.dispatch('open')

    if (this.onOpen) {
      this.onOpen()
    }
  }
}

export {Checkout}
